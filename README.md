# Wordpress with docker
A simple project w/ docker-compose.yml to start a wordpress container w/ database. It also starts a phpmyadmin container.

# Getting started
Make sure docker is installed and daemon is running, the execute:
```
docker-compose up
```

# Accessing website
In order to access the wordpress and phpmyadmin files from browser, the container IP address is necessary. Execute this to get the docker machine IP address:
```
docker-machine ls
```
It will output something like this:
```
NAME      ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER    ERRORS
default   -        virtualbox   Running   tcp://192.168.99.100:2376           v1.11.1  
```
Copy the IP address from URL column and paste in the browser, appending the proper ports:
* wordpress - http://192.168.99.100:8080
* phpmyadmin - http://192.168.99.100:8181

Note: database root password is `password`
